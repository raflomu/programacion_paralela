#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define N 3 // tama?o de matriz

int main () {


  int memsize = N*sizeof(float);
	
  float *a = (float *) malloc (memsize);
	float *c = (float *) malloc (memsize);
 
  memsize *=N;
  
	float *b = (float *) malloc (memsize);

  
	for (int i=0; i<N*N ; i++){
		
	b[i]=i+1; // se puede cambiar por un scaner

	}	
 
 	for (int i=0; i<N ; i++){
		
	a[i]=i+1; // se puede cambiar por un scaner

	}	
 
 	omp_set_num_threads(4);
  #pragma omp parallel for
  for (int i=0; i<N; i++){
    c[i]=0;
    for (int j=0; j<N; j++){
    c[i] += a[j] * b[i*N+j];
    }
  }
 
	for (int j=0; j<N ; j++){
    
	  printf("%f ",c[j]); 
    
  }		

	  
}