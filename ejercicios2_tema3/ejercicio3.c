#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define N 3000

int main () {

	int memsize = N*sizeof(float);
	float *a = (float *) malloc (memsize);
	float *b = (float *) malloc (memsize);
  float total = 0.0f;
 
	for (int i=0;i<N;i++){
		a[i]=b[i]=1.0f;
	}	
  
  omp_set_num_threads(4);
  #pragma omp parallel for
	for (int i=0;i<N;i++){
    #pragma omp critical
		total += a[i]*b[i];
	}

  printf ("%f \n",total);
 
 }
 
 