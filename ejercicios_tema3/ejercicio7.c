#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

static long num_steps = 100000;

double step;

void main ()
{

  int i;
  double x;
  double pi;
  double sum = 0.0;
  
  step = 1.0/(double) num_steps;
  
  #pragma omp parallel for private(i,x) shared(step) reduction (+:sum)
  for (i=1;i<=num_steps;i++){
    x=(i-0.5)*step;
    sum = sum +4.0/(1.0+x*x);
  }
  
  pi = step * sum;
  printf("pi = %lf\n",pi);

}