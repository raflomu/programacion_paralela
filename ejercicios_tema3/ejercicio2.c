#include <stdio.h>
#include <omp.h>

int main (){

  int hilos = 4;
  int valor_de_entrada = 3;
  int valor_de_salida = 3;
 
  omp_set_num_threads(hilos);
  #pragma omp parallel firstprivate(valor_de_entrada) // permite que entre el valor ya declarado de fuera de la region paralela
  {
  
   printf("valor de entrada = %d\n",valor_de_entrada);
   
   #pragma omp for lastprivate(valor_de_salida) // permite que valores de bucles salgan de la region paralela como si se ubiera ejecutado en secuencial
   for (valor_de_salida=0; valor_de_salida<10; valor_de_salida++){}
  
  }
  
  printf("valor de salida = %d\n", valor_de_salida);
 
	return 0;


}
