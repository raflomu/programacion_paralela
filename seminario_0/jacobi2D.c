#include <stdio.h>
#include <stdlib.h>

#define N 8 // tamaño de matriz
#define NUM_ITER 10

void inicializa_matriz (float * a, float * b){

  for (int i=0; i<N; i++) {
    for (int j=0; j<N; j++) {
    
    int index = (i*N)+j;
    a[index] = b[index] = (i==0||i==(N-1)||(j==0)||(j==N-1)) ? 150.0f : 70.0f;
    
    }
  }
  }
  
void imprime_matriz (float * a){

  	for (int i=0; i<N ; i++){
   		for (int j=0; j<N ; j++){
    
   		printf("%f ",a[(i*N)+j]); 
      
  		 }		
    
   	printf("\n");
    
  	}	
   }
   
void jacobi2D (float *a, float *b){

    for (int i=1; i<N-1; i++) {
      for (int j=1; j<N-1; j++) {
    
    int index = (i*N)+j;
    b[index] = (a[index]+a[index-N]+a[index+N]+a[index+1]+a[index-1])/5.0f;
    
    }
  }
}

int main () {

	int size = N*N*sizeof(float);

	float *matriz1 = (float *) malloc (size);
	float *matriz2 = (float *) malloc (size);

  inicializa_matriz (matriz1, matriz2);

	for (int i=0; i<NUM_ITER; i++){
  
  jacobi2D (matriz1,matriz2);
  float * aux = matriz1;
  matriz1=matriz2;
  matriz2=aux;
  
  }
 
  imprime_matriz(matriz2);
 

	  
}
