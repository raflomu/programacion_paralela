#include <stdio.h>
#include <stdlib.h>

#define N 3 // tama?o de matriz

int main () {

	int size = N*N*sizeof(int);

	int *matriz1 = (int *) malloc (size);
	int *matriz2 = (int *) malloc (size);
	int *matriz3 = (int *) malloc (size);

  
	for (int i=0; i<N*N ; i++){
		
	matriz1[i]=i+1; // se puede cambiar por un scaner
	matriz2[i]=i+1; // se puede cambiar por un scaner

	}	
 
 	for (int i=0; i<N ; i++){
   		for (int j=0; j<N ; j++){
       			 for (int k=0; k<N ; k++){
   		  
          matriz3[(i*N)+j] += matriz1[(i*N)+k]*matriz2[(k*N)+j];        
        
        		  }
  		 }		
  	}	
 
	for (int i=0; i<N ; i++){
   		for (int j=0; j<N ; j++){
    
   		printf("%d ",matriz3[(i*N)+j]); 
      
  		 }		
    
   	printf("\n");
    
  	}	
	  
}
