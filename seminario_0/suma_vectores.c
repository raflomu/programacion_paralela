#include <stdio.h>
#include <stdlib.h>

#define N 8

int main () {

	int memsize = N*sizeof(float);
	float *a = (float *) malloc (memsize);
	float *b = (float *) malloc (memsize);
	float *c = (float *) malloc (memsize);

	for (int i=0;i<N;i++){
		a[i]=b[i]=c[i]=1.0f;
	}	

	for (int i=0;i<N;i++){
		c[i] = a[i]+b[i];
	}

	for (int i=0;i<N;i++){
		printf ("%f, ",c[i]);
	}


}
