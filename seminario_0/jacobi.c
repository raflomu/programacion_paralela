#include <stdio.h>
#include <stdlib.h>

#define N 8
#define NUM_ITER 100

int main () {

	int memsize = N*sizeof(float);
	float *a = (float *) malloc (memsize);
 	float *b = (float *) malloc (memsize);
  	
 	 b[0] = a[0]=150.0f;
 	 b[N-1] = a[N-1]=150.0f;
  
	for (int i=1;i<N-1;i++){
		
	a[i]=70.0f;

	}	
	
	for (int j=0; j<NUM_ITER; j++){
	for (int i=1;i<N-1;i++){

	 b[i]=((a[i-1]+a[i]+a[i+1])/3);   	 

	}

	float * aux = a;
	a=b;
	b=aux;

        } 		
 

	for (int i=0;i<N;i++){

	 printf ("%f, ",b[i]);

	}


}
