#include <stdio.h>
#include <stdlib.h>

#define N 8

int main () {

	int memsize = N*sizeof(float);
	float *a = (float *) malloc (memsize);
	float b = 0;

	for (int i=0;i<N;i++){
		a[i]=1.0f;
	}	

	for (int i=0;i<N;i++){
		b += a[i];
	}

	printf ("%f",b);
	


}
