#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define N 3 // tama?o de matriz

int main () {

	int size = N*N*sizeof(int);

	int *matriz1 = (int *) malloc (size);
	int *matriz2 = (int *) malloc (size);
	int *matriz3 = (int *) malloc (size);
  int aux; 

  
	for (int i=0; i<N*N ; i++){
		
	matriz1[i]=i+1; // se puede cambiar por un scaner
	matriz2[i]=i+1; // se puede cambiar por un scaner

	}
  
  double start = omp_get_wtime();	
 
  //omp_set_num_threads(8);
  #pragma omp parallel for private(aux)
 	for (int i=0; i<N ; i++){
   		for (int j=0; j<N ; j++){
      aux = 0;
       			 for (int k=0; k<N ; k++){
   		  
                aux += matriz1[(i*N)+k]*matriz2[(k*N)+j];        
  
            }
            
      matriz3[(i*N)+j] = aux;
      
  		 }		
  	}
    
  double end = omp_get_wtime();	
  printf("El tiempo de ejecucion es %f\n",end-start);	
 
	for (int i=0; i<N ; i++){
   		for (int j=0; j<N ; j++){
    
   		printf("%d ",matriz3[(i*N)+j]); 
      
  		 }		
    
   	printf("\n");
    
  	}	
	  
}