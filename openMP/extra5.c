#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define N 1024

int main () {

	int memsize = N*sizeof(float);
	float *a = (float *) malloc (memsize);  
	float *b = (float *) malloc (memsize);  
	float total = 0.0f; 

	for (int i=0;i<N; ++i){
		a[i]=b[i]=1.0f;
	}

	omp_set_num_threads (4);
  float * total_aux = (float *) malloc (4*sizeof(float));
	#pragma omp parallel for private(total)
	for (int i=0;i<N; ++i) {
		total_aux[omp_get_thread_num()] += a[i]*b[i];
	}
  
  	for (int i=0;i<4; ++i) {
		total += total_aux[omp_get_thread_num()];
	}	
	
	printf ("%f", total);
}






