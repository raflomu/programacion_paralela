#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define N 8

int main () {

	int memsize = N*sizeof(float);
	float *a = (float *) malloc (memsize);
	float *b = (float *) malloc (memsize);
	float *c = (float *) malloc (memsize);
  float cons = 2.0f;
    
	for (int i=0;i<N;i++){
		a[i]=1.0f;
    b[i]=1.0f;
    c[i]=1.0f;
    
	}	
  
  omp_set_num_threads(4);
  #pragma omp parallel for firstprivate(cons)
	for (int i=0;i<N;i++){
 
  c[i] = cons*a[i]+b[i];
  
	}
 
 	for (int i=0;i<N;i++){
	 printf("%f ",c[i]);
	}


}