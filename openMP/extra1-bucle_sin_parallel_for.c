#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define N 8

int main () {

	int memsize = N*sizeof(float);
	float *a = (float *) malloc (memsize);
	float *b = (float *) malloc (memsize);
	float *c = (float *) malloc (memsize);
    
	for (int i=0;i<N;i++){
		a[i]=b[i]=c[i]=1.0f;
	}	
  
  omp_set_num_threads(4);
  #pragma omp parallel sections
{
   #pragma omp section
   {
     for (int i=0;i<(N/4);i++)
     c[i] = a[i]+b[i];   
   } 
   #pragma omp section
   {
     for (int i=(N/4);i<(N/2);i++)
     c[i] = a[i]+b[i];   
   } 
   #pragma omp section
   {
     for (int i=(N/2);i<((N*3)/4);i++)
     c[i] = a[i]+b[i];  
   } 
   #pragma omp section
   {
     for (int i=((N*3)/4);i<N;i++)
     c[i] = a[i]+b[i];
   } 
} 
   
	for (int i=0;i<N;i++){
		printf ("%f, ",c[i]);
	}


}
