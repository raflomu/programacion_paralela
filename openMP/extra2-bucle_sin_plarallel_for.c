#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define N 8

int main () {

	int memsize = N*sizeof(float);
	float *a = (float *) malloc (memsize);
	float *b = (float *) malloc (memsize);
	float *c = (float *) malloc (memsize);
    
	for (int i=0;i<N;i++){
		a[i]=b[i]=c[i]=1.0f;
	}	
  
  omp_set_num_threads(4);
  #pragma omp parallel 
  {
    int id = omp_get_thread_num();
    printf("El hilo con id %d\n",id);
   
   /* 
    c[id] = a[id]+b[id];
    id+=omp_get_num_threads();
    c[id] = a[id] + b[id];
    */
    
	  for (int i=id;i<N;i+=omp_get_num_threads()){
      printf("soy el hilo %d y mi i = %d\n",id,i);
		  c[i] = a[i]+b[i];
	  }
 }

	for (int i=0;i<N;i++){
		printf ("%f, ",c[i]);
	}


}