#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define N 8

int main () {

	int memsize = N*sizeof(float);
	float *a = (float *) malloc (memsize);
	float *b = (float *) malloc (memsize);
	float *c = (float *) malloc (memsize);
  float temp = 0.0f;
    
	for (int i=0;i<N;i++){
		a[i]=4.0f;
    b[i]=2.0f;
    c[i]=0.0f;
    
	}	
  
  //omp_set_num_threads(4);
  #pragma omp parallel for shared(a,b,c) private(temp)
	for (int i=0;i<N;i++){
  temp = a[i]/b[i];
  c[i] = temp + temp * temp;
	}
 
 	for (int i=0;i<N;i++){
	 printf("%f ",c[i]);
	}


}
