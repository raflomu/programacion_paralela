#include <stdio.h>
#include <omp.h>

int main (){

  int numthreads = omp_get_num_threads();
  int id = omp_get_thread_num();

	//printf ("El numero de hilos es %d y el identificador es %d\n",numthreads,id);
 
  omp_set_num_threads(4);
  #pragma omp parallel shared (id)
  {
  
  numthreads = omp_get_num_threads();
  id = omp_get_thread_num();
  printf ("El numero de hilos es %d y el identificador es %d\n",numthreads,id);  
  
  }
 
	return 0;


}
